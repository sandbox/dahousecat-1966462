
-- SUMMARY --

Twitter fetch fetches tweets from Twitter and can display them in a block, or make them available for other modules to use. 
It does not rely on and other modules, such as OAUTH so is lightweight. 

For a full description of the module, visit the project page:
http:////drupal.org/project/twitter_fetch

To submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/twitter_fetch


-- REQUIREMENTS --

Drupal 7.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions.

* Customize module settings in Administration » Web Services » Twitter Fetch.

* To control how many tweets appear in the block vist the blocks configuration page.


-- CUSTOMIZATION --

* To change the HTML output in :

  1) Copy twitter_fetch.tpl.php out of the module folder to your theme folder and make any changes required.



-- CONTACT --

Author & maintainer:
* Felix Eve - http://drupal.org/user/1410992
