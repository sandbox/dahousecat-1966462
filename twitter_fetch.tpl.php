<ul class="twitter_fetch">
  
  <?php
  $x=1; 
  foreach($tweets as $tweet) { 
    $classes = array();
    if($x==1) $classes[] = 'first';
    if($x==$num_tweets) $classes[] = 'last';
    $class = $classes ? ' class="'.implode(' ', $classes).'"' : '';
    ?>
      
    <li<?php echo $class; ?>>
      <a href="<?php echo $tweet->link; ?>"><img src="<?php echo $tweet->image_url; ?>" /></a>
      <p><?php echo $tweet->title; ?></p>
      <em><?php echo date('d/m/Y H:i', strtotime($tweet->pubDate)); ?></em>
    </li>
    
    <?php
    $x++; 
  } 
  ?>
  
</ul>